//
//  Loader.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 02/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Loader: NSObject {
    fileprivate var activityIndicator: NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .pacman, color: UIColor.init((red: 234.0, green: 0.0, blue: 38.0)), padding: NVActivityIndicatorView.DEFAULT_PADDING)
    fileprivate let bgView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
    
    func startLoaderAtCenter(view: UIView) {
        if (!view.subviews.contains(activityIndicator)) {
            bgView.backgroundColor = .black
            bgView.alpha = 0.3
            view.addSubview(bgView)
            activityIndicator.center = view.center
            view.addSubview(activityIndicator)
        } else {
            bgView.isHidden = false
        }
        
        activityIndicator.startAnimating()
    }
    
    func stopLoader() {
        activityIndicator.stopAnimating()
        bgView.isHidden = true
    }
}
