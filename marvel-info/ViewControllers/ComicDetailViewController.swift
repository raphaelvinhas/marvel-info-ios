//
//  ComicDetailViewController.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit

class ComicDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Outlets and Variables
    @IBOutlet weak var comicImage: UIImageView!
    @IBOutlet weak var comicDescription: UILabel!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var charactersTitleLabel: UILabel!
    @IBOutlet weak var notAvailableLabel: UILabel!
    
    var comic : ComicsModel = ComicsModel()
    fileprivate var characters : [CharactersModel] = [CharactersModel]()
    fileprivate let loader : Loader = Loader()
    fileprivate var page = 0
    fileprivate var total : Int = 0
    fileprivate var shouldFetch: Bool = true
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hero.isEnabled = true
        setupLayoutAndInfos()
        getCharacters()
    }
    
    // MARK: - Helpers
    fileprivate func setupLayoutAndInfos() {
        self.title = comic.title
        comicImage.hero.id = comic.title
        comicImage.af_setImage(withURL: URL(string: comic.thumbnail)!)
        comicDescription.text = comic.descriptionText
    }
    
    fileprivate func setupNoCharsView() {
        self.notAvailableLabel.isHidden = false
        self.tableViewHeightConstraint.constant = 90
    }
    
    // MARK: - Requests
    fileprivate func getCharacters() {
        loader.startLoaderAtCenter(view: view)
        print(comic.id)
        CharacterManager().getCharactersFromComic(comicId: comic.id, page: page, success: { (response) in
            print("resposta\(response.characters.count)")
            self.characters = response.characters
            self.total = response.total
            self.tableView.reloadData()
            self.tableViewHeightConstraint.constant = CGFloat(70 * self.characters.count + 20)
            self.loader.stopLoader()
            if (self.characters.count==0) {
                self.setupNoCharsView()
            }
            self.shouldFetch = true
        }) { (error) in
            self.loader.stopLoader()
            let alert = UIAlertController(title: "Erro", message: error.domain, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - TableView Datasource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComicCell") as! ComicTableViewCell
        
        cell.setupWithImageAndName(imageURL: characters[indexPath.row].thumbnail, name: characters[indexPath.row].name)
        
        return cell
    }
    
    // MARK: - ScrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height && characters.count*page < total && shouldFetch{
            
            shouldFetch = false
            page += 1
            getCharacters()
            self.tableView.reloadData()
        }
    }

}
