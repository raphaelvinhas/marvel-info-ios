//
//  HomeViewController.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Outlets and Variables
    fileprivate var selectedIndex : Int = 0
    fileprivate var shouldTransitionCells : Bool = true
    fileprivate var shouldFetch: Bool = true
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var comicsList : [ComicsModel] = [ComicsModel]()
    fileprivate var page: Int = 0
    fileprivate var total: Int = 0
    fileprivate let loader: Loader = Loader()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        loader.startLoaderAtCenter(view: view)
        setupRefresh()
        getComics()
    }
    
    // MARK: - Refresh Control
    fileprivate func setupRefresh() {
        self.refreshControl = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refreshControl.tintColor = UIColor.red
        self.refreshControl.addTarget(self, action: #selector(refreshStream), for: .valueChanged)
        self.collectionView!.refreshControl = refreshControl
    }
    
    fileprivate @objc func refreshStream() {
        page = 0
        getComics()
    }
    
    // MARK: - Requests
    fileprivate func getComics() {
        ComicsManager().getComics(page: page, success: { (responseComics) in
            if (self.refreshControl.isRefreshing == true) {
                self.comicsList = [ComicsModel]()
            }
            self.comicsList.append(contentsOf: responseComics.comics)
            self.collectionView.reloadData()
            self.loader.stopLoader()
            self.total = responseComics.total
            self.shouldFetch = true
            self.refreshControl?.endRefreshing()
        }) { (error) in
            self.loader.stopLoader()
            let alert = UIAlertController(title: "Erro", message: error.domain, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    // MARK: - Navigation Helpers
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ComicDetailViewController
        destinationVC.comic = comicsList[selectedIndex]
    }
    
    
    // MARK: - CollectionView Datasource and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comicsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCollectionViewCell
        
        cell.setupCellWithImageAndName(comicsList[indexPath.row].thumbnail, comicsList[indexPath.row].title)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.size.width/3 - 12, height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "ComicDetailSegue", sender: self)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if shouldTransitionCells {
        
            cell.transform = CGAffineTransform(scaleX: 0, y: 0)
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                
                cell.transform = .identity
                
            }, completion: { (finished) in
                
                if self.collectionView!.visibleCells.count >= indexPath.row-1 {
                    
                    self.shouldTransitionCells = false
                }
            })
        }
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height && comicsList.count*page < total && shouldFetch{
            
            shouldFetch = false
            page += 1
            getComics()
            self.collectionView.reloadData()
        }
    }
}
