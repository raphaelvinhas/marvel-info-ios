//
//  StringExtension.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import Foundation

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
