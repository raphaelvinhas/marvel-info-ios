//
//  ColorExtension.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 02/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit

extension UIColor {
    // Init with RGB/RGBA
    // swiftlint:disable large_tuple
    convenience init(_ redgreenblue: (red: CGFloat, green: CGFloat, blue: CGFloat)) {
        self.init(red: redgreenblue.red/255, green: redgreenblue.green/255, blue: redgreenblue.blue/255, alpha: 1)
    }
    // swiftlint:disable large_tuple
    convenience init(_ redgreenblueaalpha: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)) {
        self.init(red: redgreenblueaalpha.red/255, green: redgreenblueaalpha.green/255, blue: redgreenblueaalpha.blue/255, alpha: redgreenblueaalpha.alpha)
    }
    
    // Init with HEX
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let alpha, red, green, blue: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (alpha, red, green, blue) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (alpha, red, green, blue) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (alpha, red, green, blue) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (alpha, red, green, blue) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: CGFloat(alpha) / 255)
    }
}
