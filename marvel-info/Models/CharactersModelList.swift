//
//  CharactersModelList.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import SwiftyJSON

class CharactersModelList: NSObject {
    var characters : [CharactersModel] = [CharactersModel]()
    var total : Int = 0
    
    convenience init(dataJSON: JSON) {
        self.init()
        characters = [CharactersModel]()
        
        if dataJSON["data"].dictionary != nil {
            total = (dataJSON["data"].dictionary!["total"]?.intValue)!
            let contentArray = dataJSON["data"].dictionary!["results"]?.array
            for charactersAux in contentArray! {
                let model = CharactersModel(dataJSON: charactersAux)
                characters.append(model)
            }
        }
    }
}
