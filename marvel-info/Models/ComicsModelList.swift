//
//  ComicsModelList.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import SwiftyJSON

class ComicsModelList: NSObject {
    var comics : [ComicsModel] = [ComicsModel]()
    var total: Int = 0
    
    convenience init(dataJSON: JSON) {
        self.init()
        comics = [ComicsModel]()
        
        if dataJSON["data"].dictionary != nil {
            total = (dataJSON["data"].dictionary!["total"]?.intValue)!
            let contentArray = dataJSON["data"].dictionary!["results"]?.array
            for comicsAux in contentArray! {
                let model = ComicsModel(dataJSON: comicsAux)
                comics.append(model)
            }
        }
    }
}
