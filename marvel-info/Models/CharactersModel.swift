//
//  CharactersModel.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import SwiftyJSON

class CharactersModel: NSObject {
    var name : String = ""
    var id : Int = 0
    var descriptionText: String = ""
    var thumbnail: String = ""
    
    convenience init(dataJSON: JSON) {
        self.init()
        name = dataJSON["name"].stringValue
        id = dataJSON["id"].intValue
        descriptionText = dataJSON["description"].stringValue
        thumbnail = (dataJSON["thumbnail"].dictionary!["path"]?.stringValue)!+"."+(dataJSON["thumbnail"].dictionary!["extension"]?.stringValue)!
    }
}
