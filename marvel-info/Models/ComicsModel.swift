//
//  ComicsModel.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import SwiftyJSON

class ComicsModel: NSObject {
    var title : String = ""
    var id : Int = 0
    var descriptionText: String = ""
    var thumbnail: String = ""
    
    convenience init(dataJSON: JSON) {
        self.init()
        title = dataJSON["title"].stringValue
        id = dataJSON["id"].intValue
        if dataJSON["description"].stringValue == "" {
            descriptionText = "Não disponível"
        } else {
            descriptionText = dataJSON["description"].stringValue
        }
        thumbnail = (dataJSON["thumbnail"].dictionary!["path"]?.stringValue)!+"."+(dataJSON["thumbnail"].dictionary!["extension"]?.stringValue)!
    }
}
