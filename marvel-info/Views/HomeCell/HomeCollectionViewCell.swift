//
//  HomeCollectionViewCell.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import AlamofireImage
import Hero

class HomeCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet fileprivate weak var backgroundImage: UIImageView!
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Helpers
    func setupCellWithImageAndName(_ bgImage: String, _ comicName: String) {
        backgroundImage.af_setImage(withURL: URL(string: bgImage)!, placeholderImage: UIImage(named: "camera-placeholder"), filter: nil, progress: nil, imageTransition: UIImageView.ImageTransition.crossDissolve(0.5), runImageTransitionIfCached: false, completion: nil)
        backgroundImage.hero.id = comicName
    }

}
