//
//  ComicTableViewCell.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit

class ComicTableViewCell: UITableViewCell {

    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    fileprivate func setupLayout() {
        circleImage.layer.cornerRadius = circleImage.frame.size.width/2
    }
    
    func setupWithImageAndName(imageURL: String, name: String) {
        circleImage.af_setImage(withURL: URL(string: imageURL)!, placeholderImage: UIImage(named: "camera-placeholder"), filter: nil, progress: nil, imageTransition: UIImageView.ImageTransition.crossDissolve(0.5), runImageTransitionIfCached: false, completion: nil)
        nameLabel.text = name
    }

}
