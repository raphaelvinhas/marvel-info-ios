//
//  ApiManager.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 30/04/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CryptoSwift

class ApiManager: NSObject {
    // MARK: - Variables
    var header : HTTPHeaders = ["content-type" : "application/json"]
    
    // MARK: - Requests
    
    
    /// Do GET requests and returns a JSON Object
    ///
    /// - Parameters:
    ///   - endpoint: Endpoint url
    ///   - request: parameters of request
    ///   - success: success block to execute
    ///   - failure: failure block to execute
    func GET (endpoint: String, request: [String: Any]?, success: @escaping(JSON) -> Void, failure: @escaping(NSError) -> Void) {
        if isNetworkOk(failure: failure) {
            Alamofire.request("base.url".localized()+endpoint, method: .get, parameters: getDefaultParams(), encoding: URLEncoding.default, headers: getHeader())
                .responseJSON(completionHandler: { (response) in
                    self.manageResponse(success: success, failure: failure, response: response)
                })
        }
    }
    
    /// Do POST requests and returns a JSON Object
    ///
    /// - Parameters:
    ///   - endpoint: Endpoint url
    ///   - request: parameters of request
    ///   - success: success block to execute
    ///   - failure: failure block to execute
    func POST (endpoint: String, request: [String: Any]?, success: @escaping(JSON) -> Void, failure: @escaping(NSError) -> Void) {
        if isNetworkOk(failure: failure) {
            Alamofire.request("base.url".localized()+endpoint, method: .post, parameters: request, encoding: JSONEncoding.default, headers: getHeader())
                .responseJSON(completionHandler: { (response) in
                    self.manageResponse(success: success, failure: failure, response: response)
                })
        }
    }
    
    /// Do PUT requests and returns a JSON Object
    ///
    /// - Parameters:
    ///   - endpoint: Endpoint url
    ///   - request: parameters of request
    ///   - success: success block to execute
    ///   - failure: failure block to execute
    func PUT (endpoint: String, request: [String: Any]?, success: @escaping(JSON) -> Void, failure: @escaping(NSError) -> Void) {
        if isNetworkOk(failure: failure) {
            Alamofire.request("base.url".localized()+endpoint, method: .put, parameters: request, encoding: JSONEncoding.default, headers: getHeader())
                .responseJSON(completionHandler: { (response) in
                    self.manageResponse(success: success, failure: failure, response: response)
                })
        }
    }
    
    /// Do DELETE requests and returns a JSON Object
    ///
    /// - Parameters:
    ///   - endpoint: Endpoint url
    ///   - request: parameters of request
    ///   - success: success block to execute
    ///   - failure: failure block to execute
    func DELETE (endpoint: String, request: [String: Any]?, success: @escaping(JSON) -> Void, failure: @escaping(NSError) -> Void) {
        if isNetworkOk(failure: failure) {
            Alamofire.request("base.url".localized()+endpoint, method: .delete, parameters: request, encoding: JSONEncoding.default, headers: getHeader())
                .responseJSON(completionHandler: { (response) in
                    self.manageResponse(success: success, failure: failure, response: response)
                })
        }
    }
    
    // MARK: - Helpers
    
    func getHeader() -> HTTPHeaders {
        /* Add any other header */
        
        return header
    }
    
//    func getFinalEndpointWithTSAndHash() -> String {
//        let timestamp = Date().timeIntervalSince1970
//        let hash = "hash="+MD5("\(timestamp)"+"594c30b01d767c3e2ec31c8ea01767025e8088e5"+"a1a4386de7031ba7bc6339766ea5b645")
//        
//        return "&ts=\(timestamp)&\(hash)"
//    }
    
    func manageResponse (success: @escaping(JSON) -> Void, failure: @escaping(NSError) -> Void, response: DataResponse<Any>) {
        switch (response.result) {
        case .success :
            do {
                let json = try JSON(data: response.data!)
                success(json)
            } catch {
                failure(NSError(domain: "error.unknown".localized(), code: 0, userInfo: nil))
            }
            
        case .failure :
            let error = NSError(domain: "", code:response.response?.statusCode ?? 0, userInfo: nil)
            failure(error)
        }
    }
    
    func getDefaultParams() -> [String: String] {
        let timestamp = Int64(Date().timeIntervalSince1970)
        let hash = "\(timestamp)\(Constants.Keys.privateApiKey)\(Constants.Keys.publicApiKey)".md5()
        let params : [String: String] = ["hash":hash, "ts": "\(timestamp)", "apikey":Constants.Keys.publicApiKey]
        
        return params
    }
    
    
    /// Check the network status
    ///
    /// - Parameter failure: failure block to execute in case of something is wrong with the network
    /// - Returns: if the network is ok to continue with the request
    func isNetworkOk (failure: @escaping(NSError) -> Void) -> Bool {
        // Check network connection
        switch Reach().connectionStatus() {
        case .offline:
            failure(NSError(domain: "error.offline".localized(), code: 0, userInfo: nil))
            return false
        case .unknown:
            failure(NSError(domain: "error.unknown".localized(), code: 500, userInfo: nil))
            return false
        default:
            return true
        }
    }
}
