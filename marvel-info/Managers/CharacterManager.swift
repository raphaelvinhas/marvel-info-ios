//
//  CharacterManager.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit

class CharacterManager: ApiManager {
    func getCharactersFromComic(comicId: Int, page: Int, success: @escaping(CharactersModelList) -> Void, failure: @escaping(NSError) -> Void) {
        let finalEndpoint = "comics.characters.url".localized().replacingOccurrences(of: "{offset}", with: "\((page)*20)").replacingOccurrences(of: "{comicId}", with: "\(comicId)")
        
        self.GET(endpoint: finalEndpoint, request: nil, success: { (response) in
            let characters = CharactersModelList(dataJSON: response)
            success(characters)
        }) { (error) in
            failure(error)
        }
    }
}
