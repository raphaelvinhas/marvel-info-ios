//
//  ComicsManager.swift
//  marvel-info
//
//  Created by Raphael Vinhas on 01/05/18.
//  Copyright © 2018 RPV. All rights reserved.
//

import UIKit
import SwiftyJSON

class ComicsManager: ApiManager {
    func getComics(page: Int, success: @escaping(ComicsModelList) -> Void, failure: @escaping(NSError) -> Void) {
        let finalEndpoint = "comics.url".localized().replacingOccurrences(of: "{offset}", with: "\((page)*20)")
        
        self.GET(endpoint: finalEndpoint, request: nil, success: { (response) in
            let comics = ComicsModelList(dataJSON: response)
            success(comics)
        }) { (error) in
            failure(error)
        }
    }
    
    func getComicsFromCharacter(charId: Int, page: Int, success: @escaping(ComicsModelList) -> Void, failure: @escaping(NSError) -> Void) {
        let finalEndpoint = "character.comics.url".localized().replacingOccurrences(of: "{offset}", with: "\((page)*20)").replacingOccurrences(of: "{characterId}", with: "\(charId)")
        
        self.GET(endpoint: finalEndpoint, request: nil, success: { (response) in
            let comics = ComicsModelList(dataJSON: response)
            success(comics)
        }) { (error) in
            failure(error)
        }
    }
}
